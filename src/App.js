import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Menu from './components/Menu';
import Home from './components/Home';
import Video from './components/Video';
import Account from './components/Account';
import Auth from './components/Auth';
import ReactRouter from './components/ReactRouter';
import Error from './components/Error';
import Postid from './components/Postid';
import Wellcome from './components/Wellcome';

function App() {
  return (
    <div>
        <BrowserRouter>
         <Menu/>
        <Switch>
          <Route exact path="/" component={ReactRouter}/>
          <Route path="/home" component={Home}/>
          <Route path="/video" component={Video}/>
          <Route path="/account" component ={Account}/>
          <Route path ="/auth" component={Auth}/>
          <Route path="/postid/:id" component={Postid}/>
          <Route path="/acount"component={Account}/>
          <Route path="/wellcome" component={Wellcome}/>
          <Route path="*" component={Error}/>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
