import React, { Component } from 'react';
import {Nav,Navbar,Form,FormControl,Button} from 'react-bootstrap';
import {Link} from 'react-router-dom'

export default class Menu extends Component {
    render() {
        return (
            <div >
                <Navbar bg="light" variant="light">
                <Navbar.Brand as={Link} to="/">React Router</Navbar.Brand>
                <Nav className="mr-auto">
                <Nav.Link as={Link} to="/home">Home</Nav.Link>
                <Nav.Link as={Link} to="/video">Video</Nav.Link>
                <Nav.Link as={Link} to="/account">Account</Nav.Link>
                <Nav.Link as={Link} to="auth">Auth</Nav.Link>
                </Nav>
                <Form inline>
                <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                <Button variant="outline-primary">Search</Button>
                </Form>
            </Navbar>
            </div>
        )
    }
}
