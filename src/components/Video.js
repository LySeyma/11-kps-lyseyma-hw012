import React from 'react'
import { Link, Route } from 'react-router-dom'

const Animation =()=>(
    <div>
        <h2>Animation Category</h2>
        <ul>
            <li><Link to ={"/video/animation/action"}>Action</Link></li>
            <li><Link to ={"/video/animation/romantic"}>Romantion</Link></li>
            <li><Link to ={"/video/animation/comedy"}>Comdey</Link></li>
        </ul>
        
        <Route path={"/video/animation/:AnimationsName"} component={DetailAnimation}/>
    </div>
)
const DetailAnimation =(props)=>(
    <div>
        <h2>{props.match.params.AnimationsName}</h2>
    </div>
)
const Movies =()=>(
    <div>
        <h2>Movies Category</h2>
        <ul>
            <li><Link to={"/video/movies/adventuer"}>Adventuer</Link></li>
            <li><Link to={"/video/movies/comdey"}>Comdey</Link></li>
            <li><Link to={"/video/movies/crime"}>Crime</Link></li>
            <li><Link to={"/video/movies/doucment"}>Doucment</Link></li>
        </ul>
        <Route path={"/video/movies/:MoviesName"} component={DetailMovies}/>
    </div>
)
const DetailMovies =(props)=>(
    <div>
        <h2>{props.match.params.MoviesName}</h2>
    </div>
)
const VideoList=()=>(
    <div>
        <ul>     
            <li><Link to={`/video/animation`}>Animation</Link></li>
            <li><Link to={`/video/movies`}>Movies</Link></li>
        </ul>

    </div>
)
// const DetailVideoList =(props)=>(
//     <div>
//         <h2>Please Select A Topic !</h2>
//     </div>
// )
function Video(props) {
    const option ="Please Select A Topic";

 
    return (
        <div>
            <Route path={"/video"} component={VideoList}/>
            <Route path={"/video/animation"}  component={Animation}/>
            <Route path={"/video/movies"} component={Movies}/>
            {/* <Route path={"/video"} component={DetailVideoList}/> */}
            
            
            <h2>{option}</h2>
            
                
        </div>
        
    )
}
export default Video;

