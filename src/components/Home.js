import React from 'react';
import Cardimage from './Cardimage';
import image1 from '../assets/image1.jpg';
import image2 from '../assets/image2.jpg';
import image3 from '../assets/image3.jpg';
import image4 from '../assets/image4.jpg';
import image5 from '../assets/image5.jpg';
import image6 from '../assets/image6.jpg';
 
export default function Home() {
    return (
        <div className="container-fluid d-flex justify-content-center">
            <div className="row">
                <div className="col-md-4"> <Cardimage imgsrc={image1} title={"Rose"}/></div>
                <div className="col-md-4"><Cardimage imgsrc={image2} title={"White_Flower"}/></div>
                <div className="col-md-4"><Cardimage imgsrc={image3} title={"Magnolia"}/></div>
                <div className="col-md-4"><Cardimage imgsrc={image4} title={"Sunflower"}/></div>
                <div className="col-md-4"><Cardimage imgsrc={image5} title={"Lotus_Flower"}/></div>
                <div className="col-md-4"><Cardimage imgsrc={image6} title={"Frostproof"}/></div>
            </div>    
        </div>
    )
}
