import React from 'react'
import { Link } from 'react-router-dom';
import querysting from 'query-string';

export default function Account(props) {
    let qn=querysting.parse(props.location.search)
    let Name = "name";
    let No ="no";
    
    console.log(qn.name)
    return (
        <div>
            <h3>Account</h3>
            <ul>
                <li><Link to={`/acount?name=Netflix`}>Netflix</Link></li>
                <li><Link to={`/acount?name=Zillow_Group`}>Zillow Group</Link></li>
                <li><Link to={`/acount?name=Yahoo`}>Yahoo</Link></li>
                <li><Link to={`/acount?name=Modus_Create`}>Modus Create</Link></li>
            </ul>
             <h3>The {qn.name ? Name:No} in the query string is {qn.name}</h3>
            
        </div>
    )
}
