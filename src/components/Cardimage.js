import React from 'react';
import '../style/card-style.css';
import Moment from 'react-moment';
import 'moment/locale/en-au';
import {Link} from 'react-router-dom'

const Cardimage =props=> {
    const date = new Date();
    return (
        <div className='container'>
        <div className='card text-center'>
            <div className="overflow">
               <img src={props.imgsrc} alt="image1" className="card-img-top" />
            </div>
            <div className="card-body text-break" >
                <h4 className="card-title">{props.title}</h4>
                <p className="card-text text-secondary">
                Flowers are the reproductive parts of flowering plants. A flower is a special part of the plant. Flowers are also called the bloom or blossom of a plant. Flowers have petals. Inside the part of the flower that has petals are the parts which produce pollen and seeds.
                </p>
                <Link to={`/postid/1`} className="btn btn-outline-info">
                        Click me
                </Link>            
            </div>
            <div className="card-footer">            
                <Moment fromNow ago>{date}</Moment>
            </div>

           </div>
        </div>
    )
}
export default Cardimage;
